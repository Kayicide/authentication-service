﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using AuthenticationService.Features.ClientFeature;
using Moq;
using NUnit.Framework;

namespace AuthenticationService.UnitTests.Features.ClientFeature
{
    public class GetAllClientsRequestHandlerTests
    {
        private Mock<IRepository<Client>> _clientRepository;
        private Mock<IRepository<User>> _userRepository;
        private GetAllClientsRequestHandler _objectToTest;

        [SetUp]
        public void Setup()
        {
            _clientRepository = new Mock<IRepository<Client>>();
            _userRepository = new Mock<IRepository<User>>();

            _clientRepository.Setup(x => x.Read()).ReturnsAsync(new List<Client>{
                new Client
                {
                  Id = "Correct",
                  Name = "NotCorrect",
                  JoinToken = "Incorrect",
                  CreatedDateTime = DateTime.Now
                }, 
                new Client
                {
                    Id = "Correct",
                    Name = "ExactMatch",
                    JoinToken = "Incorrect",
                    CreatedDateTime = DateTime.Now
                },
                new Client
                {
                    Id = "Correct",
                    Name = "Match",
                    JoinToken = "Incorrect",
                    CreatedDateTime = DateTime.Now
                },
                new Client
                {
                    Id = "Correct",
                    Name = "NotCorrect",
                    JoinToken = "Join",
                    CreatedDateTime = DateTime.Now
                },
                new Client
                {
                    Id = "Correct",
                    Name = "NotCorrect",
                    JoinToken = "JoinExact",
                    CreatedDateTime = DateTime.Now
                },
                new Client
                {
                    Id = "Correct",
                    Name = "NotCorrect",
                    JoinToken = "Incorrect",
                    CreatedDateTime = DateTime.Now.AddDays(-5)
                },
                new Client
                {
                    Id = "Correct",
                    Name = "NotCorrect",
                    JoinToken = "Incorrect",
                    CreatedDateTime = DateTime.Now.AddHours(-20)
                }
            });


            _objectToTest = new GetAllClientsRequestHandler(_clientRepository.Object, _userRepository.Object);
        }

        [Test]
        public async Task ReturnsCorrectWhenFilteringOnName()
        {
            var request = new GetAllClientsRequest
            {
                Name = "Match",
                NameExact = false,
                LowerCreatedDateTime = DateTime.MinValue,
                UpperCreatedDateTime = DateTime.MinValue
            };

            var result = await _objectToTest.HandleRequest(request);

            Assert.That(result.Count, Is.EqualTo(2));
        }

        [Test]
        public async Task ReturnsCorrectWhenFilteringOnNameExact()
        {
            var request = new GetAllClientsRequest
            {
                Name = "ExactMatch",
                NameExact = true,
                LowerCreatedDateTime = DateTime.MinValue,
                UpperCreatedDateTime = DateTime.MinValue
            };

            var result = await _objectToTest.HandleRequest(request);

            Assert.That(result.Count, Is.EqualTo(1));
        }

        [Test]
        public async Task ReturnsCorrectWhenFilteringOnJointToken()
        {
            var request = new GetAllClientsRequest
            {
                JoinToken = "JoinExact",
                LowerCreatedDateTime = DateTime.MinValue,
                UpperCreatedDateTime = DateTime.MinValue
            };

            var result = await _objectToTest.HandleRequest(request);

            Assert.That(result.Count, Is.EqualTo(1));
        }

        [Test]
        public async Task ReturnsCorrectWhenFilteringOnJointDateTime()
        {
            var request = new GetAllClientsRequest
            {
                LowerCreatedDateTime = DateTime.Now.AddDays(-1),
                UpperCreatedDateTime = DateTime.Now.AddDays(1)
            };

            var result = await _objectToTest.HandleRequest(request);

            Assert.That(result.Count, Is.EqualTo(6));
        }
    }
}
