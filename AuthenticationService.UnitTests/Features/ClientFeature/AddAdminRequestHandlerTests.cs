﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using AuthenticationService.Features.ClientFeature;
using Microsoft.AspNetCore.Mvc.Diagnostics;
using Moq;
using NUnit.Framework;

namespace AuthenticationService.UnitTests.Features.ClientFeature
{
    [TestFixture]
    public class AddAdminRequestHandlerTests
    {
        private Mock<IRepository<Client>> _clientRepository;
        private Mock<IRepository<User>> _userRepository;
        private AddAdminRequestHandler _objectToTest;

        [SetUp]
        public void Setup()
        {
            _clientRepository = new Mock<IRepository<Client>>();
            _userRepository = new Mock<IRepository<User>>();

            _userRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new User { Id = "User" });
            _clientRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new Client { Admins = new List<string> { "User" }, Owner = "ThisUser" });

            _objectToTest = new AddAdminRequestHandler(_clientRepository.Object, _userRepository.Object);
        }

        [Test]
        public async Task InvalidUserIdReturnsNull()
        {
            _userRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(() => (User) null);
            var result = await _objectToTest.HandleRequest(new AddAdminRequest {UserId = "test", ClientId = "test", CurrentUser = "test"});

            Assert.That(result, Is.Null);
        }

        [Test]
        public async Task InvalidClientIdReturnsNull()
        {
            _userRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new User { Id = "User" });
            _clientRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(() => (Client) null);
            var result = await _objectToTest.HandleRequest(new AddAdminRequest { UserId = "User", ClientId = "NotClient", CurrentUser = "test" });

            Assert.That(result, Is.Null);
        }

        [Test]
        public async Task UserIsUnauthorised()
        {
            _userRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new User { Id = "User" });
            _clientRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new Client{Admins = new List<string>{"NotThisUser"}, Owner = "NotThisUser"});
            var result = await _objectToTest.HandleRequest(new AddAdminRequest { UserId = "User", ClientId = "Client", CurrentUser = "User" });

            Assert.That(result, Is.Null);
        }

        [Test]
        public async Task AllValuesCorrectReturnsCorrectResponse()
        {
            var result = await _objectToTest.HandleRequest(new AddAdminRequest { UserId = "User", ClientId = "Client", CurrentUser = "ThisUser" });

            Assert.That(result, Is.Not.Null);
        }
    }
}
