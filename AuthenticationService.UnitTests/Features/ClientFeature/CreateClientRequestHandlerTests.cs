﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using AuthenticationService.Features;
using AuthenticationService.Features.ClientFeature;
using AuthenticationService.Features.Utility;
using Moq;
using NUnit.Framework;

namespace AuthenticationService.UnitTests.Features.ClientFeature
{
    public class CreateClientRequestHandlerTests
    {
        private Mock<IRepository<Client>> _clientRepository;
        private Mock<IRepository<User>> _userRepository;
        private Mock<IRequestHandler<GenerateTokenRequest, string>> _generateTokenRequest;
        private CreateClientRequestHandler _objectToTest;

        [SetUp]
        public void Setup()
        {
            _clientRepository = new Mock<IRepository<Client>>();
            _userRepository = new Mock<IRepository<User>>();
            _generateTokenRequest = new Mock<IRequestHandler<GenerateTokenRequest, string>>();

            _generateTokenRequest.Setup(x => x.HandleRequest(It.IsAny<GenerateTokenRequest>())).ReturnsAsync("1234567890");

            _objectToTest = new CreateClientRequestHandler(_clientRepository.Object, _userRepository.Object, _generateTokenRequest.Object);
        }

        [Test]
        public async Task InvalidUserIdReturnsNull()
        {
            _userRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(() => (User) null);
            var result = await _objectToTest.HandleRequest(new CreateClientRequest {UserId = "Test", Name = "Test", Type = ClientType.Team});

            Assert.That(result, Is.Null);
        }

        [Test]
        public async Task ClientIdAddedToUser()
        {
            var user = new User {Id = "User"};
            _userRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(user);
            _clientRepository.Setup(x => x.Create(It.IsAny<Client>())).ReturnsAsync(new Client {Id = "ClientId", Name = "ClientName"});
            var result = await _objectToTest.HandleRequest(new CreateClientRequest { UserId = "User", Name = "Test", Type = ClientType.Team });

            Assert.That(user.ClientIds.Count, Is.EqualTo(1));
            Assert.That(user.ClientIds.First(), Is.EqualTo("ClientId"));
        }

        [Test]
        public async Task AllValuesCorrectReturnsCorrectResponse()
        {
            _userRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new User { Id = "User" });
            _clientRepository.Setup(x => x.Create(It.IsAny<Client>())).ReturnsAsync(new Client { Id = "ClientId", Name = "ClientName" });
            var result = await _objectToTest.HandleRequest(new CreateClientRequest { UserId = "User", Name = "Test", Type = ClientType.Team });

            Assert.That(result, Is.Not.Null);
        }
    }
}
