﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using AuthenticationService.Features;
using AuthenticationService.Features.ClientFeature;
using AuthenticationService.Features.Utility;
using Moq;
using NUnit.Framework;

namespace AuthenticationService.UnitTests.Features.ClientFeature
{
    public class RegenerateJoinTokenRequestHandlerTests
    {
        private Mock<IRepository<Client>> _clientRepository;
        private Mock<IRequestHandler<GenerateTokenRequest, string>> _generateTokenRequestHandler;
        private RegenerateJoinTokenRequestHandler _objectToTest;

        [SetUp]
        public void Setup()
        {
            _clientRepository = new Mock<IRepository<Client>>();
            _generateTokenRequestHandler = new Mock<IRequestHandler<GenerateTokenRequest, string>>();

            _objectToTest = new RegenerateJoinTokenRequestHandler(_clientRepository.Object, _generateTokenRequestHandler.Object);
        }

        [Test]
        public async Task InvalidClientIdReturnsNull()
        {
            _clientRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(() => (Client) null);

            var result = await _objectToTest.HandleRequest(new RegenerateJoinTokenRequest{ClientId = "", CurrentUserId = ""});
            Assert.That(result, Is.Null);
        }

        [Test]
        public async Task GivenCorrectInputsResultIsCorrect()
        {
            _clientRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new Client { Admins = new List<string> { "Lol" }, Owner = "LOL", JoinToken = "JoinToken"});
            _generateTokenRequestHandler.Setup(x => x.HandleRequest(It.IsAny<GenerateTokenRequest>())).ReturnsAsync("1234567890");

            var result = await _objectToTest.HandleRequest(new RegenerateJoinTokenRequest { ClientId = "Test", CurrentUserId = "Lol" });
            Assert.That(result, Is.Not.Null);
            Assert.That(result.JoinToken, Is.EqualTo("1234567890"));
        }
    }
}
