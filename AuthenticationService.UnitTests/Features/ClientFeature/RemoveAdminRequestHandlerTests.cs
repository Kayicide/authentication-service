﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using AuthenticationService.Features.ClientFeature;
using EllipticCurve;
using Microsoft.AspNetCore.Razor.Language.CodeGeneration;
using MongoDB.Bson;
using Moq;
using NUnit.Framework;

namespace AuthenticationService.UnitTests.Features.ClientFeature
{
    [TestFixture]
    public class RemoveAdminRequestHandlerTests
    {
        private string _userId;

        private Mock<IRepository<User>> _userRepository;
        private Mock<IRepository<Client>> _clientRepository;

        private RemoveAdminRequestHandler _objectToTest;
        [SetUp]
        public void Setup()
        {
            _userRepository = new Mock<IRepository<User>>();
            _clientRepository = new Mock<IRepository<Client>>();


            _userId = ObjectId.GenerateNewId().ToString();
            _userRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new User{Id = "test"});
            _clientRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new Client{Owner = _userId, Admins = new List<string>{"test"}});

            _objectToTest = new RemoveAdminRequestHandler(_clientRepository.Object, _userRepository.Object);
        }
        [Test]
        public async Task InvalidUserIdReturnsNull()
        {
            _userRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync((User) null);
            var result = await _objectToTest.HandleRequest(new RemoveAdminRequest { ClientId = "", UserId = "test", CurrentUser = _userId });

            Assert.That(result, Is.Null);
        }
        [Test]
        public async Task InvalidClientIdReturnsNull()
        {
            _clientRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync((Client) null);
            var result = await _objectToTest.HandleRequest(new RemoveAdminRequest { ClientId = "", UserId = "test", CurrentUser = _userId });

            Assert.That(result, Is.Null);
        }
        [Test]
        public async Task CurrentUserIsNotOwnerReturnsNull()
        {
            var result = await _objectToTest.HandleRequest(new RemoveAdminRequest { ClientId = "", UserId = "test", CurrentUser = "test" });

            Assert.That(result, Is.Null);
        }

        [Test]
        public async Task CorrectDataReturnsCorrectResult()
        {
            var result = await _objectToTest.HandleRequest(new RemoveAdminRequest{ClientId = "", UserId = "test", CurrentUser = _userId});

            Assert.That(result, !Is.Null);
        }
    }
}
