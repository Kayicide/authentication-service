﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using AuthenticationService.Features.ClientFeature;
using MongoDB.Bson;
using Moq;
using NUnit.Framework;

namespace AuthenticationService.UnitTests.Features.ClientFeature
{
    public class SwitchOwnershipRequestHandlerTests
    {
        private Mock<IRepository<Client>> _clientRepository;
        private Mock<IRepository<User>> _userRepository;

        private string _userId;
        private SwitchOwnershipRequestHandler _objectToTest;
        [SetUp]
        public void Setup()
        {
            _clientRepository = new Mock<IRepository<Client>>();
            _userRepository = new Mock<IRepository<User>>();

            _userId = ObjectId.GenerateNewId().ToString();

            _clientRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new Client{Owner = _userId, Admins = new List<string>()});
            _userRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new User{ Id = "NewOwner" });

            _objectToTest = new SwitchOwnershipRequestHandler(_clientRepository.Object, _userRepository.Object);
        }

        [Test]
        public async Task InvalidUserIdReturnsNull()
        {
            _userRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync((User) null);
            var result = await _objectToTest.HandleRequest(new SwitchOwnershipRequest{UserId = "Test", ClientId = "Test", CurrentUser = _userId});

            Assert.That(result, Is.Null);
        }

        [Test]
        public async Task InvalidClientIdReturnsNull()
        {
            _clientRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync((Client)null);
            var result = await _objectToTest.HandleRequest(new SwitchOwnershipRequest { UserId = "Test", ClientId = "Test", CurrentUser = _userId });

            Assert.That(result, Is.Null);
        }

        [Test]
        public async Task OwnerIsSwitched()
        {
            var result = await _objectToTest.HandleRequest(new SwitchOwnershipRequest { UserId = "Test", ClientId = "Test", CurrentUser = _userId });

            Assert.That(result, !Is.Null);
            Assert.That(result.Owner, Is.EqualTo("NewOwner"));
        }
    }
}
