﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using AuthenticationService.Features;
using AuthenticationService.Features.EmailConfirmationFeature;
using AuthenticationService.Features.UserFeature;
using AuthenticationService.Utility;
using Moq;
using NUnit.Framework;

namespace AuthenticationService.UnitTests.Features.UserFeature
{
    public class RegisterUserRequestHandlerTests
    {
        private Mock<IRepository<User>> _userRepository;
        private Mock<IRequestHandler<SendConfirmationEmailRequest, bool>> _sendConfirmationEmailRequest;
        private Mock<IRequestHandler<CreateEmailConfirmationRequest, EmailConfirmation>> _createEmailConfirmationRequestHandler;

        private RegisterUserRequestHandler _objectToTest;

        [SetUp]
        public void Setup()
        {
            _userRepository = new Mock<IRepository<User>>();
            _sendConfirmationEmailRequest = new Mock<IRequestHandler<SendConfirmationEmailRequest, bool>>();
            _createEmailConfirmationRequestHandler = new Mock<IRequestHandler<CreateEmailConfirmationRequest, EmailConfirmation>>();

            _userRepository.Setup(x => x.Read()).ReturnsAsync(new List<User> {new User {Email = "User"}});
            _sendConfirmationEmailRequest.Setup(x => x.HandleRequest(It.IsAny<SendConfirmationEmailRequest>())).ReturnsAsync(true);
            _createEmailConfirmationRequestHandler
                .Setup(x => x.HandleRequest(It.IsAny<CreateEmailConfirmationRequest>()))
                .ReturnsAsync(new EmailConfirmation());

            _objectToTest = new RegisterUserRequestHandler(_userRepository.Object, _sendConfirmationEmailRequest.Object, _createEmailConfirmationRequestHandler.Object);
        }

        [Test]
        public async Task IfAccountAlreadyExistsNullIsReturned()
        {
            var result = await _objectToTest.HandleRequest(new RegisterUserRequest { FirstName = "", HashSalt = new SaltedHash.HashSalt(), Email = "User", LastName = "" });

            Assert.That(result, Is.Null);
        }

        [Test]
        public async Task UserReturnedIfDataCorrect()
        {
            var result = await _objectToTest.HandleRequest(new RegisterUserRequest{FirstName = "", HashSalt = new SaltedHash.HashSalt(), Email = "new", LastName = ""});

            Assert.That(result, !Is.Null);
        }
    }
}
