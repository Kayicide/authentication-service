﻿using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using AuthenticationService.Features.UserFeature;
using AuthenticationService.Utility;
using Moq;
using NUnit.Framework;

namespace AuthenticationService.UnitTests.Features.UserFeature
{
    [TestFixture]
    public class UpdateUserRequestHandlerTests
    {
        private Mock<IRepository<User>> _userRepository;
        private UpdateUserRequestHandler _objectToTest;

        [SetUp]
        public void Setup()
        {
            _userRepository = new Mock<IRepository<User>>();

            _userRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new User());

            _objectToTest = new UpdateUserRequestHandler(_userRepository.Object);
        }

        [Test]
        public async Task InvalidUserIdReturnsNull()
        {
            _userRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync((User) null);

            var result = await _objectToTest.HandleRequest(new UpdateUserRequest {FirstName = "test"});

            Assert.That(result, Is.Null);
        }

        [Test]
        public async Task PasswordUpdatedCorrectly()
        {
            _userRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new User{HashSalt = new SaltedHash.HashSalt{Hash = "HashCurrent", Salt = "SaltCurrent"}});

            var result = await _objectToTest.HandleRequest(new UpdateUserRequest { FirstName = "test" , Salt = "Salt", Hash = "Hash"});

            Assert.That(result, !Is.Null);
            Assert.That(result.HashSalt.Hash, Is.EqualTo("Hash"));
            Assert.That(result.HashSalt.Salt, Is.EqualTo("Salt"));
        }

        [Test]
        public async Task UserDeletedFlagSet()
        {
            _userRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new User{Deleted = false});

            var result = await _objectToTest.HandleRequest(new UpdateUserRequest { Deleted = true});

            Assert.That(result, !Is.Null);
            Assert.That(result.Deleted, Is.EqualTo(true));
        }

        [Test]
        public async Task CorrectInfoReturnsCorrectResult()
        {
            _userRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new User());

            var result = await _objectToTest.HandleRequest(new UpdateUserRequest { FirstName = "test" });

            Assert.That(result, !Is.Null);
            Assert.That(result.FirstName, Is.EqualTo("test"));
        }
    }
}
