﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using AuthenticationService.Features.UserFeature;
using Moq;
using NUnit.Framework;

namespace AuthenticationService.UnitTests.Features.UserFeature
{
    public class AddUserToClientRequestHandlerTests
    {
        private Mock<IRepository<Client>> _clientRepository;
        private Mock<IRepository<User>> _userRepository;

        private AddUserToClientRequestHandler _objectToTest;

        [SetUp]
        public void SetUp()
        {
            _clientRepository = new Mock<IRepository<Client>>();
            _userRepository = new Mock<IRepository<User>>();

            _clientRepository.Setup(x => x.Read()).ReturnsAsync(new List<Client> {new() {JoinToken = "test"}});
            _userRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new User());

            _objectToTest = new AddUserToClientRequestHandler(_clientRepository.Object, _userRepository.Object);
        }

        [Test]
        public async Task InvalidUserIdReturnsFalse()
        {
            _userRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync((User) null);
            var result = await _objectToTest.HandleRequest(new AddUserToClientRequest{UserId = "test", ClientJoinToken = "test"});

            Assert.That(result, Is.False);
        }

        [Test]
        public async Task InvalidJoinTokenReturnsFalse()
        {
            var result = await _objectToTest.HandleRequest(new AddUserToClientRequest { UserId = "test", ClientJoinToken = "NOTtest" });

            Assert.That(result, Is.False);
        }

        [Test]
        public async Task TrueReturnedWhenDataCorrect()
        {
            var result = await _objectToTest.HandleRequest(new AddUserToClientRequest { UserId = "test", ClientJoinToken = "test" });

            Assert.That(result, Is.True);
        }

    }
}
