﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using AuthenticationService.Features.UserFeature;
using Moq;
using NUnit.Framework;

namespace AuthenticationService.UnitTests.Features.UserFeature
{
    public class RemoveUserFromClientRequestHandlerTests
    {
        private Mock<IRepository<User>> _userRepository;
        private Mock<IRepository<Client>> _clientRepository;

        private RemoveUserFromClientRequestHandler _objectToTest;

        [SetUp]
        public void Setup()
        {
            _userRepository = new Mock<IRepository<User>>();
            _clientRepository = new Mock<IRepository<Client>>();

            _userRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new User{ Id = "UserLeaving", ClientIds = new List<string>{"Client"}});
            _clientRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new Client{Id = "Client"});

            _objectToTest = new RemoveUserFromClientRequestHandler(_userRepository.Object, _clientRepository.Object);
        }

        [Test]
        public async Task UserIdInvalidReturnsFalse()
        {
            _userRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync((User)null);
            var result = await _objectToTest.HandleRequest(new RemoveUserFromClientRequest{ClientId = "", CurrentUserId = "UserLeaving", UserId = ""});

            Assert.That(result, Is.False);
        }

        [Test]
        public async Task ClientIdInvalidReturnsFalse()
        {
            _clientRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync((Client)null);
            var result = await _objectToTest.HandleRequest(new RemoveUserFromClientRequest { ClientId = "", CurrentUserId = "UserLeaving", UserId = "" });

            Assert.That(result, Is.False);
        }

        [Test]
        public async Task KickingUserReturnsTrue()
        {
            _clientRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new Client { Id = "Client", Admins = new List<string>{"Admin"}});
            var result = await _objectToTest.HandleRequest(new RemoveUserFromClientRequest { ClientId = "", CurrentUserId = "Admin", UserId = "UserLeaving" });

            Assert.That(result, Is.True);
        }
        [Test]
        public async Task KickingUserWhileUnauthorisedReturnsFalse()
        {
            _clientRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new Client { Id = "Client", Admins = new List<string> { "Admin" }, Owner = ""});
            var result = await _objectToTest.HandleRequest(new RemoveUserFromClientRequest { ClientId = "", CurrentUserId = "NotAdmin", UserId = "UserLeaving" });

            Assert.That(result, Is.False);
        }
    }
}
