﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using AuthenticationService.Features.UserFeature;
using Moq;
using NUnit.Framework;

namespace AuthenticationService.UnitTests.Features.ClientFeature
{
    public class PromoteUserToAdminRequestHandlerTests
    {
        private Mock<IRepository<User>> _userRepository;

        private PromoteUserToAdminRequestHandler _objectToTest;
        [SetUp]
        public void Setup()
        {
            _userRepository = new Mock<IRepository<User>>();
            _userRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new User());

            _objectToTest = new PromoteUserToAdminRequestHandler(_userRepository.Object);
        }

        [Test]
        public async Task InvalidUserIdReturnsNull()
        {
            _userRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync((User)null);
            var result = await _objectToTest.HandleRequest(new PromoteUserToAdminRequest {UserId = "test"});

            Assert.That(result, Is.Null);
        }
        [Test]
        public async Task CorrectInfoReturnsCorrectResponse()
        {
            var result = await _objectToTest.HandleRequest(new PromoteUserToAdminRequest { UserId = "test" });

            Assert.That(result, !Is.Null);
        }

    }
}
