﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using LoggingService.Data;
using MongoDB.Driver;

namespace AuthenticationService.Data.Repositories
{
    public class ClientRepository : IRepository<Client>
    {
        private readonly IMongoCollection<Client> _clients;

        public ClientRepository(IDbAccessor databaseAccessor)
        {
            _clients = databaseAccessor.Database.GetCollection<Client>("clients");
        }

        public async Task<Client> Create(Client client)
        {
            await _clients.InsertOneAsync(client);
            return client;
        }

        public async Task<IList<Client>> Read() =>
            (await _clients.FindAsync(sub => true)).ToList();

        public async Task<Client> Find(string id)
        {
            var result = (await _clients.FindAsync(aClient => aClient.Id == id)).SingleOrDefault();
            return result;
        }

        public async Task Update(Client client) =>
            await _clients.ReplaceOneAsync(aClient => aClient.Id == client.Id, client);

        public async Task Delete(string id) =>
            await _clients.DeleteOneAsync(aClient => aClient.Id == id);
    }
}

