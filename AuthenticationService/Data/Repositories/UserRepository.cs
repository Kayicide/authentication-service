﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using LoggingService.Data;
using MongoDB.Driver;

namespace AuthenticationService.Data.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly IMongoCollection<User> _users;

        public UserRepository(IDbAccessor databaseAccessor)
        {
            _users = databaseAccessor.Database.GetCollection<User>("users");
        }

        public async Task<User> Create(User user)
        {
            await _users.InsertOneAsync(user);
            return user;
        }

        public async Task<IList<User>> Read() =>
            (await _users.FindAsync(sub => true)).ToList();

        public async Task<User> Find(string id) =>
            (await _users.FindAsync(aUser => aUser.Id == id)).SingleOrDefault();

        public async Task Update(User user) =>
            await _users.ReplaceOneAsync(aUser => aUser.Id == user.Id, user);

        public async Task Delete(string id) =>
            await _users.DeleteOneAsync(aUser => aUser.Id == id);
    }
}
