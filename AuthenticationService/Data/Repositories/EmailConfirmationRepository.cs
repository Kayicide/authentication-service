﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using LoggingService.Data;
using MongoDB.Driver;

namespace AuthenticationService.Data.Repositories
{
    public class EmailConfirmationRepository : IRepository<EmailConfirmation>
    {
        private readonly IMongoCollection<EmailConfirmation> _emailConfirmations;

        public EmailConfirmationRepository(IDbAccessor databaseAccessor)
        {
            _emailConfirmations = databaseAccessor.Database.GetCollection<EmailConfirmation>("email_confirmations");
        }

        public async Task<EmailConfirmation> Create(EmailConfirmation client)
        {
            await _emailConfirmations.InsertOneAsync(client);
            return client;
        }

        public async Task<IList<EmailConfirmation>> Read() =>
            (await _emailConfirmations.FindAsync(sub => true)).ToList();

        public async Task<EmailConfirmation> Find(string id) =>
            (await _emailConfirmations.FindAsync(emailConfirmation => emailConfirmation.UserId == id)).SingleOrDefault();

        public async Task Update(EmailConfirmation emailConfirmation) =>
            await _emailConfirmations.ReplaceOneAsync(x => x.UserId == emailConfirmation.UserId, emailConfirmation);

        public async Task Delete(string id) =>
            await _emailConfirmations.DeleteOneAsync(emailConfirmation => emailConfirmation.UserId == id);
    }
}

