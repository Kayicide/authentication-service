﻿using MongoDB.Driver;

namespace AuthenticationService.Data
{
    public interface IDbAccessor
    {
        IMongoDatabase Database { get; }
    }
}
