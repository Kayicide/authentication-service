﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace AuthenticationService.Data.Entity
{
    public class EmailConfirmation
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string UserId { get; set; }
        public Guid Token { get; set; }
        public bool Confirmed { get; set; }
        public DateTime CreatedDateTime { get; set; }
    }
}
