﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using LoggingService.Features;

namespace AuthenticationService.Features.ClientFeature
{
    public class GetClientRequest
    {
        public string ClientId { get; set; }
    }
    public class GetClientRequestHandler : IRequestHandler<GetClientRequest, Client>
    {
        private readonly IRepository<Client> _repository;
        public GetClientRequestHandler(IRepository<Client> repository)
        {
            _repository = repository;
        }
        public async Task<Client> HandleRequest(GetClientRequest request)
        {
            return await _repository.Find(request.ClientId);
        }
    }
}
