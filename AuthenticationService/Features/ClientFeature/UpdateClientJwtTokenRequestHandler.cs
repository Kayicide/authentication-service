﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Data;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using LoggingService.Features;

namespace AuthenticationService.Features.ClientFeature
{
    public class UpdateClientJwtTokenRequest : IAuthedRequest
    {
        public string CurrentUserId { get; set; }
        public string ClientId { get; set; }
        public string JwtToken { get; set; }
    }
    public class UpdateClientJwtTokenRequestHandler : IRequestHandler<UpdateClientJwtTokenRequest, Client>
    {
        private readonly IRepository<Client> _repository;
        public UpdateClientJwtTokenRequestHandler(IRepository<Client> repository)
        {
            _repository = repository;
        }
        public async Task<Client> HandleRequest(UpdateClientJwtTokenRequest request)
        {
            var client = await _repository.Find(request.ClientId);
            if (client == null)
                return null;

            client.ApiToken = request.JwtToken;
            await _repository.Update(client);

            return client;
        }
    }
}
