﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Data;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using AuthenticationService.Features.Utility;
using LoggingService.Features;

namespace AuthenticationService.Features.ClientFeature
{
    public class RegenerateJoinTokenRequest : IAuthedRequest
    {
        public string CurrentUserId { get; set; }
        public string ClientId { get; set; }
    }
    public class RegenerateJoinTokenRequestHandler : IRequestHandler<RegenerateJoinTokenRequest, Client>
    {
        private readonly IRepository<Client> _repository;
        private readonly IRequestHandler<GenerateTokenRequest, string> _generateTokenRequestHandler;
        public RegenerateJoinTokenRequestHandler(IRepository<Client> repository, IRequestHandler<GenerateTokenRequest, string> generateTokenRequestHandler)
        {
            _repository = repository;
            _generateTokenRequestHandler = generateTokenRequestHandler;
        }
        public async Task<Client> HandleRequest(RegenerateJoinTokenRequest request)
        {
            var client = await _repository.Find(request.ClientId);
            if (client == null)
                return null;

            client.JoinToken = await _generateTokenRequestHandler.HandleRequest(new GenerateTokenRequest {Length = 10});

            await _repository.Update(client);
            return client;
        }
    }
}
