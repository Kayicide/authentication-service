﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Controllers.HttpRequests;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using AuthenticationService.Features.Utility;
using LoggingService.Features;

namespace AuthenticationService.Features.ClientFeature
{
    public class CreateClientRequest
    {
        public string Name { get; set; }
        public ClientType Type { get; set; }
        public string UserId { get; set; }
    }
    public class CreateClientRequestHandler : IRequestHandler<CreateClientRequest, Client>
    {
        private readonly IRepository<Client> _clientRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IRequestHandler<GenerateTokenRequest, string> _generateTokenRequestHandler;
        public CreateClientRequestHandler(IRepository<Client> clientRepository, IRepository<User> userRepository, IRequestHandler<GenerateTokenRequest, string> generateTokenRequestHandler)
        {
            _clientRepository = clientRepository;
            _userRepository = userRepository;
            _generateTokenRequestHandler = generateTokenRequestHandler;
        }
        public async Task<Client> HandleRequest(CreateClientRequest request)
        {
            var admins = new List<string> {request.UserId};
            var joinToken = await _generateTokenRequestHandler.HandleRequest(new GenerateTokenRequest{Length = 10});
            var client = new Client
            {
                Name = request.Name,
                Type = request.Type,
                Admins = admins,
                JoinToken = joinToken,
                ApiToken = "",
                Active = true,
                Owner = request.UserId,
                CreatedDateTime = DateTime.Now
            };
            client = await _clientRepository.Create(client);

            var user = await _userRepository.Find(request.UserId);
            if (user == null)
                return null;

            user.ClientIds ??= new List<string>();
            user.ClientIds.Add(client.Id);
            await _userRepository.Update(user);

            return client;
        }
    }
}
