﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Data;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using AuthenticationService.Features.Utility;
using LoggingService.Features;

namespace AuthenticationService.Features.ClientFeature
{
    public class UpdateClientNameRequest : IAuthedRequest
    { 
        public string CurrentUserId { get; set; }
        public string ClientId { get; set; }
        public string Name { get; set; }
        public bool? Active { get; set; }
    }
    public class UpdateClientNameRequestHandler : IRequestHandler<UpdateClientNameRequest, Client>
    {
        private readonly IRepository<Client> _clientRepository;
        private readonly IRepository<User> _userRepository;
        public UpdateClientNameRequestHandler(IRepository<Client> clientRepository, IRepository<User> userRepository)
        {
            _clientRepository = clientRepository;
            _userRepository = userRepository;
        }
        public async Task<Client> HandleRequest(UpdateClientNameRequest request)
        {
            var user = await _userRepository.Find(request.CurrentUserId);
            if(user == null)
                return null;

            var client = await _clientRepository.Find(request.ClientId);
            if (client == null)
                return null;

            if (request.Active.HasValue && (client.Owner.Equals(request.CurrentUserId) || user.Role.Equals("Admin")))
                client.Active = request.Active.Value;

            if(!string.IsNullOrEmpty(request.Name))
                client.Name = request.Name;

            await _clientRepository.Update(client);
            return client;
        }
    }
}
