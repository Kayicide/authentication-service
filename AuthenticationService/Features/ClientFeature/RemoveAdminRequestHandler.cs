﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using LoggingService.Features;

namespace AuthenticationService.Features.ClientFeature
{
    public class RemoveAdminRequest
    {
        public string CurrentUser { get; set; }
        public string ClientId { get; set; }
        public string UserId { get; set; }
    }
    public class RemoveAdminRequestHandler : IRequestHandler<RemoveAdminRequest, Client>
    {
        private readonly IRepository<Client> _clientRepository;
        private readonly IRepository<User> _userRepository;
        public RemoveAdminRequestHandler(IRepository<Client> clientRepository, IRepository<User> userRepository)
        {
            _clientRepository = clientRepository;
            _userRepository = userRepository;
        }
        public async Task<Client> HandleRequest(RemoveAdminRequest request)
        {
            var user = await _userRepository.Find(request.UserId);
            if (user == null)
                return null;

            var client = await _clientRepository.Find(request.ClientId);
            if (client == null)
                return null;

            if (!client.Owner.Equals(request.CurrentUser, StringComparison.CurrentCultureIgnoreCase))
                return null;

            client.Admins.Remove(user.Id);
            await _clientRepository.Update(client);

            return client;
        }
    }
}
