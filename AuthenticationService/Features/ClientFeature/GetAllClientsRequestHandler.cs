﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using LoggingService.Features;

namespace AuthenticationService.Features.ClientFeature
{
    public class GetAllClientsRequest : IRequest
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public bool NameExact { get; set; }
        public string JoinToken { get; set; }
        public bool? Active { get; set; }
        public DateTime LowerCreatedDateTime { get; set; }
        public DateTime UpperCreatedDateTime { get; set; }
    }
    public class GetAllClientsRequestHandler : IRequestHandler<GetAllClientsRequest, IList<Client>>
    {
        private readonly IRepository<Client> _clientRepository;
        private readonly IRepository<User> _userRepository;
        public GetAllClientsRequestHandler(IRepository<Client> clientRepository, IRepository<User> userRepository)
        {
            _clientRepository = clientRepository;
            _userRepository = userRepository;
        }
        public async Task<IList<Client>> HandleRequest(GetAllClientsRequest request)
        {
            var clients = await _clientRepository.Read();

            if (request.Active.HasValue)
                clients = clients.Where(x => x.Active == request.Active.Value).ToList();

            if (!string.IsNullOrEmpty(request.UserId))
            {
                var user = await _userRepository.Find(request.UserId);
                clients = clients.Where(x=> user.ClientIds.Contains(x.Id, StringComparer.InvariantCultureIgnoreCase)).ToList();
            }

            if (!string.IsNullOrEmpty(request.Name))
                clients = request.NameExact ? clients.Where(x => x.Name.Equals(request.Name, StringComparison.InvariantCultureIgnoreCase)).ToList() : clients.Where(x => x.Name.Contains(request.Name, StringComparison.InvariantCultureIgnoreCase)).ToList();

            if (!string.IsNullOrEmpty(request.JoinToken))
                clients = clients.Where(x => x.JoinToken.Equals(request.JoinToken)).ToList();

            if (request.LowerCreatedDateTime != DateTime.MinValue)
                clients = clients.Where(x => x.CreatedDateTime >= request.LowerCreatedDateTime).ToList();

            if(request.UpperCreatedDateTime != DateTime.MinValue)
                clients = clients.Where(x => x.CreatedDateTime <= request.UpperCreatedDateTime).ToList();

            return clients;
        }
    }
}
