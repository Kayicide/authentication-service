﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using LoggingService.Features;

namespace AuthenticationService.Features.ClientFeature
{
    public class SwitchOwnershipRequest
    {
        public string ClientId { get; set; }
        public string UserId { get; set; }
        public string CurrentUser { get; set; }
    }
    public class SwitchOwnershipRequestHandler : IRequestHandler<SwitchOwnershipRequest, Client>
    {
        private readonly IRepository<Client> _clientRepository;
        private readonly IRepository<User> _userRepository;

        public SwitchOwnershipRequestHandler(IRepository<Client> clientRepository, IRepository<User> userRepository)
        {
            _clientRepository = clientRepository;
            _userRepository = userRepository;
        }
        public async Task<Client> HandleRequest(SwitchOwnershipRequest request)
        {
            var user = await _userRepository.Find(request.UserId);
            if (user == null)
                return null;

            var client = await _clientRepository.Find(request.ClientId);
            if (client == null)
                return null;

            if (!request.CurrentUser.Equals(client.Owner, StringComparison.CurrentCultureIgnoreCase))
                return null;

            client.Owner = user.Id;
            client.Admins.Add(user.Id);
            await _clientRepository.Update(client);
            
            return client;
        }
    }
}
