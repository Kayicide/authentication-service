﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using LoggingService.Features;

namespace AuthenticationService.Features.Utility
{
    public class GenerateTokenRequest
    {
        public int Length { get; set; }
    }
    public class GenerateTokenRequestHandler : IRequestHandler<GenerateTokenRequest, string>
    {
        public Task<string> HandleRequest(GenerateTokenRequest request)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                byte[] uintBuffer = new byte[sizeof(uint)];

                while (request.Length-- > 0)
                {
                    rng.GetBytes(uintBuffer);
                    uint num = BitConverter.ToUInt32(uintBuffer, 0);
                    res.Append(valid[(int)(num % (uint)valid.Length)]);
                }
            }

            return Task.FromResult(res.ToString());
        }
    }
}
