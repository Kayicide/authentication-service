﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using LoggingService.Features;

namespace AuthenticationService.Features.Utility
{
    public class UserIsAdminOfClientRequest : IRequest
    {
        public string ClientId { get; set; }
        public string UserId { get; set; }
    }
    public class UserIsAdminOfClientRequestHandler : IRequestHandler<UserIsAdminOfClientRequest, bool>
    {
        private readonly IRepository<Client> _clientRepository;
        private readonly IRepository<User> _userRepository;
        public UserIsAdminOfClientRequestHandler(IRepository<Client> clientRepository, IRepository<User> userRepository)
        {
            _clientRepository = clientRepository;
            _userRepository = userRepository;
        }
        public async Task<bool> HandleRequest(UserIsAdminOfClientRequest request)
        {
            var client = await _clientRepository.Find(request.ClientId);
            if (!client.IsAuthorised(request.UserId))
            {
                var user = await _userRepository.Find(request.UserId);
                if (!user.Role.Equals("Admin"))
                    return false;
            }
            return true;
        }
    }
}
