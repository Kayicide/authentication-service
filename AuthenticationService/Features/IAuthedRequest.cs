﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenticationService.Data
{
    public interface IAuthedRequest
    {
        public string ClientId { get; set; }
        public string CurrentUserId { get; set; }
    }
}
