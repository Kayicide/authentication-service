﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Data;
using AuthenticationService.Features.Utility;

namespace AuthenticationService.Features
{
    public class AuthAdminRequestHandlerDecorator<TRequest, TResponse> : IRequestHandler<TRequest, TResponse> where TRequest: IAuthedRequest
    {
        private readonly IRequestHandler<UserIsAdminOfClientRequest, bool> _userIsAdminOfClientRequestHandler;
        private readonly IRequestHandler<TRequest, TResponse> _decorated;
        public AuthAdminRequestHandlerDecorator(IRequestHandler<UserIsAdminOfClientRequest, bool> userIsAdminOfClientRequestHandler, IRequestHandler<TRequest, TResponse> decorated)
        {
            _userIsAdminOfClientRequestHandler = userIsAdminOfClientRequestHandler;
            _decorated = decorated;
        }
        public async Task<TResponse> HandleRequest(TRequest request)
        {
            var authed = await _userIsAdminOfClientRequestHandler.HandleRequest(new UserIsAdminOfClientRequest{UserId = request.CurrentUserId, ClientId = request.ClientId});
            if (!authed)
                return default;
            var response = await _decorated.HandleRequest(request);
            return response;
        }
    }
}
