﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using AuthenticationService.Features.EmailConfirmationFeature;
using AuthenticationService.Utility;
using LoggingService.Features;
using MongoDB.Bson;

namespace AuthenticationService.Features.UserFeature
{
    public class RegisterUserRequest : IRequest
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public SaltedHash.HashSalt HashSalt { get; set; }
    }
    public class RegisterUserRequestHandler : IRequestHandler<RegisterUserRequest, User>
    {
        private readonly IRepository<User> _repository;
        private readonly IRequestHandler<SendConfirmationEmailRequest, bool> _sendConfirmationEmailRequest;
        private readonly IRequestHandler<CreateEmailConfirmationRequest, EmailConfirmation> _createEmailConfirmationRequestHandler;

        public RegisterUserRequestHandler(IRepository<User> repository, IRequestHandler<SendConfirmationEmailRequest, bool> sendConfirmationEmailRequest, IRequestHandler<CreateEmailConfirmationRequest, EmailConfirmation> createEmailConfirmationRequestHandler)
        {
            _repository = repository;
            _sendConfirmationEmailRequest = sendConfirmationEmailRequest;
            _createEmailConfirmationRequestHandler = createEmailConfirmationRequestHandler;
        }
        public async Task<User> HandleRequest(RegisterUserRequest request)
        {
            var users = await _repository.Read();
            if (users.Count(x => x.Email.Equals(request.Email, StringComparison.InvariantCultureIgnoreCase)) != 0)
                return null;

            var user = new User
            {
                Id = ObjectId.GenerateNewId().ToString(),
                Email = request.Email,
                ClientIds = new List<string>(),
                FirstName = request.FirstName, 
                LastName =  request.LastName,
                HashSalt = request.HashSalt, 
                Deleted = false,
                Role = "User"
            };
            await _repository.Create(user);

            var emailConfirmation = await _createEmailConfirmationRequestHandler.HandleRequest(new CreateEmailConfirmationRequest(user.Id));
            await _sendConfirmationEmailRequest.HandleRequest(new SendConfirmationEmailRequest(user.Id, user.Email, user.FirstName, user.LastName, emailConfirmation.Token));

            return user;
        }
    }
}
