﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using AuthenticationService.Utility;
using LoggingService.Features;

namespace AuthenticationService.Features.UserFeature
{
    public class UpdateUserRequest : IRequest
    {
        public string UserId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Hash { get; set; }
        public string Salt { get; set; }
        public bool? Deleted { get; set; }
        public string Role { get; set; }
    }
    public class UpdateUserRequestHandler : IRequestHandler<UpdateUserRequest, User>
    {
        private readonly IRepository<User> _repository;

        public UpdateUserRequestHandler(IRepository<User> repository)
        {
            _repository = repository;
        }
        public async Task<User> HandleRequest(UpdateUserRequest request)
        {
            var user = await _repository.Find(request.UserId);
            if (user == null)
                return null;
            //user details
            if (!string.IsNullOrEmpty(request.FirstName))
                user.FirstName = request.FirstName;
            if (!string.IsNullOrEmpty(request.LastName))
                user.LastName = request.LastName;
            if (!string.IsNullOrEmpty(request.Role))
                user.Role = request.Role;
            if (request.Deleted.HasValue)
                user.Deleted = request.Deleted.Value;
            if (!string.IsNullOrEmpty(request.Email))
                user.Email = request.Email;
            //password
            if (!string.IsNullOrEmpty(request.Hash) && !string.IsNullOrEmpty(request.Salt) && request.Salt != user.HashSalt.Salt && request.Hash != user.HashSalt.Hash)
            {
                user.HashSalt = new SaltedHash.HashSalt
                {
                    Hash = request.Hash,
                    Salt = request.Salt
                };
            }

            await _repository.Update(user);
            return user;
        }
    }
}
