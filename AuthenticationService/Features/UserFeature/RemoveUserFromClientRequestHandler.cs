﻿using System;
using System.Threading.Tasks;
using AuthenticationService.Data;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using LoggingService.Features;

namespace AuthenticationService.Features.UserFeature
{
    public class RemoveUserFromClientRequest : IRequest
    {
        public string UserId { get; set; }
        public string ClientId { get; set; }
        public string CurrentUserId { get; set; }
    }

    public class RemoveUserFromClientRequestHandler : IRequestHandler<RemoveUserFromClientRequest, bool>
    {
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Client> _clientRepository;
        public RemoveUserFromClientRequestHandler(IRepository<User> userRepository, IRepository<Client> clientRepository)
        {
            _userRepository = userRepository;
            _clientRepository = clientRepository;
        }
        public async Task<bool> HandleRequest(RemoveUserFromClientRequest request)
        {
            var client = await _clientRepository.Find(request.ClientId);
            if (client == null)
                return false;

            var user = await _userRepository.Find(request.UserId);
            if (user == null)
                return false;

            //Because we want to allow the user to leave them self even if they aren't an admin we don't use the auth decorator here.
            if(!request.CurrentUserId.Equals(request.UserId, StringComparison.CurrentCultureIgnoreCase))
                if (!client.IsAuthorised(request.CurrentUserId))
                    return false;

            if (client.Admins.Contains(user.Id))
            {
                client.Admins.Remove(user.Id);
                await _clientRepository.Update(client);
            }

            user.ClientIds.Remove(request.ClientId);
            await _userRepository.Update(user);

            return true;
        }
    }
}
