﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using LoggingService.Features;

namespace AuthenticationService.Features.UserFeature
{
    public class PromoteUserToAdminRequest
    {
        public string UserId { get; set; }
    }
    public class PromoteUserToAdminRequestHandler : IRequestHandler<PromoteUserToAdminRequest, User>
    {
        private readonly IRepository<User> _repository;
        public PromoteUserToAdminRequestHandler(IRepository<User> repository)
        {
            _repository = repository;
        }
        public async Task<User> HandleRequest(PromoteUserToAdminRequest request)
        {
            var user = await _repository.Find(request.UserId);
            if (user == null)
                return null;

            user.Role = "Admin";

            await _repository.Update(user);
            return user;
        }
    }
}
