﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using LoggingService.Features;

namespace AuthenticationService.Features.UserFeature
{
    public class GetAllUsersRequest : IRequest
    {
        public GetAllUsersRequest(string clientId, string email, bool emailExact, string firstName, bool firstNameExact, string lastName, bool lastNameExact, bool includeDeleted)
        {
            ClientId = clientId;
            Email = email;
            EmailExact = emailExact;
            FirstName = firstName;
            FirstNameExact = firstNameExact;
            LastName = lastName;
            LastNameExact = lastNameExact;
            IncludeDeleted = includeDeleted;
        }
        public string ClientId { get; }
        public string Email { get; }
        public bool EmailExact { get; }
        public string FirstName { get; } 
        public bool FirstNameExact { get; } 
        public string LastName { get; } 
        public bool LastNameExact { get; } 
        public bool IncludeDeleted { get; }
    }
    public class GetAllUsersRequestHandler : IRequestHandler<GetAllUsersRequest, IList<User>>
    {
        private readonly IRepository<User> _repository;
        public GetAllUsersRequestHandler(IRepository<User> repository)
        {
            _repository = repository;
        }
        public async Task<IList<User>> HandleRequest(GetAllUsersRequest request)
        {
            var users = await _repository.Read();

            if (!request.IncludeDeleted)
                users = users.Where(x => x.Deleted == false).ToList();

            if (request.ClientId != string.Empty)
                users = users.Where(x => x.ClientIds.Contains(request.ClientId, StringComparer.InvariantCultureIgnoreCase)).ToList();

            if(request.Email != string.Empty)
                users = request.EmailExact ? users.Where(x => x.Email.Equals(request.Email, StringComparison.InvariantCultureIgnoreCase)).ToList() : users.Where(x => x.Email.Contains(request.Email, StringComparison.InvariantCultureIgnoreCase)).ToList();

            if(request.FirstName != string.Empty)
                users = request.FirstNameExact ? users.Where(x => x.FirstName.Equals(request.FirstName, StringComparison.InvariantCultureIgnoreCase)).ToList() : users.Where(x => x.FirstName.Contains(request.FirstName, StringComparison.InvariantCultureIgnoreCase)).ToList();

            if (request.LastName != string.Empty)
                users = request.LastNameExact ? users.Where(x => x.LastName.Equals(request.LastName, StringComparison.InvariantCultureIgnoreCase)).ToList() : users.Where(x => x.LastName.Contains(request.LastName, StringComparison.InvariantCultureIgnoreCase)).ToList();

            return users;
        }
    }
}
