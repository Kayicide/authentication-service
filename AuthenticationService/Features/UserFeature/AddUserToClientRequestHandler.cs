﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using LoggingService.Features;

namespace AuthenticationService.Features.UserFeature
{
    public class AddUserToClientRequest
    {
        public string ClientJoinToken { get; set; }
        public string UserId { get; set; }
    }
    public class AddUserToClientRequestHandler : IRequestHandler<AddUserToClientRequest, bool>
    {
        private readonly IRepository<Client> _clientRepository;
        private readonly IRepository<User> _userRepository;
        public AddUserToClientRequestHandler(IRepository<Client> clientRepository, IRepository<User> userRepository)
        {
            _clientRepository = clientRepository;
            _userRepository = userRepository;
        }
        public async Task<bool> HandleRequest(AddUserToClientRequest request)
        {
            var user = await _userRepository.Find(request.UserId);
            if (user == null)
                return false;

            var client = (await _clientRepository.Read()).FirstOrDefault(x => x.JoinToken.Equals(request.ClientJoinToken));
            if (client == null)
                return false;

            user.ClientIds ??= new List<string>();

            user.ClientIds.Add(client.Id);
            await _userRepository.Update(user);

            return true;
        }
    }
}
