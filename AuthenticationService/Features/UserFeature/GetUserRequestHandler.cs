﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using LoggingService.Features;

namespace AuthenticationService.Features.UserFeature
{
    public class GetUserRequest : IRequest
    {
        public string UserId { get; set; }
        public GetUserRequest(string userId)
        {
            UserId = userId;
        }
    }
    public class GetUserRequestHandler : IRequestHandler<GetUserRequest, User>
    {
        private readonly IRepository<User> _repository;
        public GetUserRequestHandler(IRepository<User> repository)
        {
            _repository = repository;
        }
        public async Task<User> HandleRequest(GetUserRequest request)
        {
            return await _repository.Find(request.UserId);
        }
    }
}
