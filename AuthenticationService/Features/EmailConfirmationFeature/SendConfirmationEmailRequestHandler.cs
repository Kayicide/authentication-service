﻿using System;
using System.Threading.Tasks;
using AuthenticationService.Utility;
using LoggingService.Features;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace AuthenticationService.Features.EmailConfirmationFeature
{
    public class SendConfirmationEmailRequest : IRequest
    {
        public string UserId { get; }
        public string Email { get; }
        public string FirstName { get; }
        public string LastName { get; }
        public Guid Token { get; }
        public SendConfirmationEmailRequest(string userId, string email, string firstName, string lastName, Guid token)
        {
            UserId = userId;
            Email = email;
            FirstName = firstName;
            LastName = lastName;
            Token = token;
        }
    }
    public class SendConfirmationEmailRequestHandler : IRequestHandler<SendConfirmationEmailRequest, bool>
    {
        private readonly AppSettings _settings;
        public SendConfirmationEmailRequestHandler(AppSettings settings)
        {
            _settings = settings;
        }
        public async Task<bool> HandleRequest(SendConfirmationEmailRequest request)
        {
            var client = new SendGridClient(_settings.MailGridAuthKey);
            var from = new EmailAddress("Scarlett@Kayde.me", "Scarlett");
            var subject = "Log Master confirmation Email";
            var to = new EmailAddress(request.Email, request.FirstName + " " + request.LastName);
            var plainTextContent = $"Navigate to {_settings.FrontUrl}/ConfirmEmail/{request.UserId}/{request.Token} to confirm your email address!";
            var htmlContent = $"Click <a href=\"{_settings.FrontUrl}/ConfirmEmail/{request.UserId}/{request.Token}\">Here</a> to confirm your email address!";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            await client.SendEmailAsync(msg);
            return true;
        }
    }
}
