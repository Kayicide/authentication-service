﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using LoggingService.Features;

namespace AuthenticationService.Features.EmailConfirmationFeature
{
    public class CreateEmailConfirmationRequest : IRequest
    {
        public string UserId { get; }
        public CreateEmailConfirmationRequest(string userId)
        {
            UserId = userId;
        }
    }
    public class CreateEmailConfirmationRequestHandler : IRequestHandler<CreateEmailConfirmationRequest, EmailConfirmation>
    {
        private readonly IRepository<EmailConfirmation> _emailConfirmationRepository;
        public CreateEmailConfirmationRequestHandler(IRepository<EmailConfirmation> emailConfirmationRepository)
        {
            _emailConfirmationRepository = emailConfirmationRepository;
        }
        public async Task<EmailConfirmation> HandleRequest(CreateEmailConfirmationRequest request)
        {

            var emailConfirmation = new EmailConfirmation
            {
                UserId = request.UserId,
                Token = Guid.NewGuid(),
                CreatedDateTime = DateTime.Now,
                Confirmed = false
            };

            return await _emailConfirmationRepository.Create(emailConfirmation);
        }
    }
}
