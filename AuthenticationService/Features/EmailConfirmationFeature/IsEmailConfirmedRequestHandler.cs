﻿using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using LoggingService.Features;

namespace AuthenticationService.Features.EmailConfirmationFeature
{
    public class IsEmailConfirmedRequest : IRequest
    {
        public string UserId { get; set; }
        public IsEmailConfirmedRequest(string userId)
        {
            UserId = userId;
        }
    }
    public class IsEmailConfirmedRequestHandler : IRequestHandler<IsEmailConfirmedRequest, bool>
    {
        private readonly IRepository<EmailConfirmation> _repository;
        public IsEmailConfirmedRequestHandler(IRepository<EmailConfirmation> repository)
        {
            _repository = repository;
        }
        public async Task<bool> HandleRequest(IsEmailConfirmedRequest request)
        {
            var emailConfirmation = await _repository.Find(request.UserId);
            return emailConfirmation.Confirmed;
        }
    }
}
