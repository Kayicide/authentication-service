﻿using System;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using LoggingService.Features;

namespace AuthenticationService.Features.EmailConfirmationFeature
{
    public class ConfirmEmailRequest : IRequest
    {
        public string UserId { get; set; }
        public Guid Token { get; set; }
        public ConfirmEmailRequest(string userId, Guid token)
        {
            UserId = userId;
            Token = token;
        }
    }
    public class ConfirmEmailRequestHandler : IRequestHandler<ConfirmEmailRequest, bool>
    {
        private readonly IRepository<EmailConfirmation> _repository;

        public ConfirmEmailRequestHandler(IRepository<EmailConfirmation> repository)
        {
            _repository = repository;
        }
        public async Task<bool> HandleRequest(ConfirmEmailRequest request)
        {
            var emailConfirmation = await _repository.Find(request.UserId);
            if (emailConfirmation.Token != request.Token)
                return false;

            emailConfirmation.Confirmed = true;
            await _repository.Update(emailConfirmation);

            return true;
        }
    }
}
