using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthenticationService.Data;
using AuthenticationService.Data.Entity;
using AuthenticationService.Data.Repositories;
using AuthenticationService.Features;
using AuthenticationService.Features.ClientFeature;
using AuthenticationService.Features.UserFeature;
using AuthenticationService.Utility;
using LoggingService.Data;
using LoggingService.Features;
using LoggingService.Utility;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SimpleInjector;

namespace AuthenticationService
{
    public class Startup
    {
        private Container _container = new();
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            _container.Options.ResolveUnregisteredConcreteTypes = false;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvcCore();
            services.AddSimpleInjector(_container, options =>
            {
                options.AddAspNetCore().AddControllerActivation();
                options.AddLogging();
            });

            InitializeContainer();

            services.Configure<DatabaseSettings>(Configuration.GetSection("DatabaseSettings"));
            services.AddSingleton<IDatabaseSettings>(x => x.GetRequiredService<IOptions<DatabaseSettings>>().Value);

            services.AddControllers();
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);
            services.AddSingleton(x => x.GetRequiredService<IOptions<AppSettings>>().Value);


            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });

            OpenApiSecurityScheme securityDefinition = new OpenApiSecurityScheme()
            {
                Name = "Bearer",
                BearerFormat = "JWT",
                Scheme = "bearer",
                Description = "Specify the authorization token.",
                In = ParameterLocation.Header,
                Type = SecuritySchemeType.Http,
            };

            OpenApiSecurityScheme securityScheme = new OpenApiSecurityScheme()
            {
                Reference = new OpenApiReference()
                {
                    Id = "jwt_auth",
                    Type = ReferenceType.SecurityScheme
                }
            };

            OpenApiSecurityRequirement securityRequirements = new OpenApiSecurityRequirement()
            {
                {securityScheme, new string[] { }},
            };

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "My API",
                    Version = "v1"
                });
                c.AddSecurityDefinition("jwt_auth", securityDefinition);
                c.AddSecurityRequirement(securityRequirements);
            });

            services.AddAuthorization();
        }
        private void InitializeContainer()
        {
            _container.Register(typeof(IRequestHandler<,>), typeof(IRequestHandler<,>).Assembly, Lifestyle.Transient);
            _container.Register(typeof(IRepository<>), typeof(IRepository<>).Assembly, Lifestyle.Scoped);
            _container.Register<IDbAccessor, DbAccessor>(Lifestyle.Singleton);

            //Any request handler with 2 or more updates/creates has a decorator to prevent unwanted data if an error happens.
            _container.RegisterDecorator<IRequestHandler<CreateClientRequest, Client>, TransactionRequestHandlerDecorator<CreateClientRequest, Client>>();
            _container.RegisterDecorator<IRequestHandler<UpdateUserRequest, User>, TransactionRequestHandlerDecorator<UpdateUserRequest, User>>();
            _container.RegisterDecorator<IRequestHandler<RemoveUserFromClientRequest, bool>, TransactionRequestHandlerDecorator<RemoveUserFromClientRequest, bool>>();
            
            //used for authentication client admins 
            _container.RegisterDecorator<IRequestHandler<UpdateClientJwtTokenRequest, Client>, AuthAdminRequestHandlerDecorator<UpdateClientJwtTokenRequest, Client>>();
            _container.RegisterDecorator<IRequestHandler<UpdateClientNameRequest, Client>, AuthAdminRequestHandlerDecorator<UpdateClientNameRequest, Client>>();
            _container.RegisterDecorator<IRequestHandler<RegenerateJoinTokenRequest, Client>, AuthAdminRequestHandlerDecorator<RegenerateJoinTokenRequest, Client>>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "AuthenticationService v1"));
            }

            app.UseHttpsRedirection();

            app.UseAuthentication();
            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
