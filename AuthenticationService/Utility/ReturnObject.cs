﻿using System;
namespace LoggingService.Utility
{
    public class ReturnObject
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
