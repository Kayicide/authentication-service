﻿namespace AuthenticationService.Utility
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public string FrontEndName { get; set; }
        public string MailGridAuthKey { get; set; }
        public string FrontUrl { get; set; }
    }
}
