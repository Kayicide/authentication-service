﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Features;
using AuthenticationService.Features.EmailConfirmationFeature;
using AuthenticationService.Features.UserFeature;
using LoggingService.Features;

namespace AuthenticationService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class EmailConfirmationController : ControllerBase
    {
        private readonly IRequestHandler<IsEmailConfirmedRequest, bool> _isEmailConfirmedRequestHandler;
        private readonly IRequestHandler<ConfirmEmailRequest, bool> _confirmConfirmationEmailRequestHandler;
        public EmailConfirmationController(IRequestHandler<IsEmailConfirmedRequest, bool> isEmailConfirmedRequestHandler, IRequestHandler<ConfirmEmailRequest, bool> confirmConfirmationEmailRequestHandler)
        {
            _isEmailConfirmedRequestHandler = isEmailConfirmedRequestHandler;
            _confirmConfirmationEmailRequestHandler = confirmConfirmationEmailRequestHandler;
        }

        [HttpGet]
        public async Task<IActionResult> IsEmailConfirmed([FromQuery] string userId)
        {
            var result = await _isEmailConfirmedRequestHandler.HandleRequest(new IsEmailConfirmedRequest(userId));
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> ConfirmEmail([FromQuery] string userId, [FromQuery] Guid token)
        {
            var result = await _confirmConfirmationEmailRequestHandler.HandleRequest(new ConfirmEmailRequest(userId, token));
            return Ok(result);
        }
    }
}
