﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Controllers.HttpRequests;
using AuthenticationService.Data.Entity;
using AuthenticationService.Features;
using AuthenticationService.Features.ClientFeature;
using AuthenticationService.Features.UserFeature;
using AuthenticationService.Utility;
using LoggingService.Features;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AuthenticationService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : Controller
    {
        private readonly IRequestHandler<GetAllUsersRequest, IList<User>> _getAllUsersRequestHandler;
        private readonly IRequestHandler<GetUserRequest, User> _getUserRequestHandler;
        private readonly IRequestHandler<RegisterUserRequest, User> _registerUserRequestHandler;
        private readonly IRequestHandler<UpdateUserRequest, User> _updateUserRequestHandler;
        private readonly IRequestHandler<AddUserToClientRequest, bool> _addUserToClientRequestHandler;
        private readonly IRequestHandler<RemoveUserFromClientRequest, bool> _removeUserFromClientRequestHandler;
        private readonly IRequestHandler<PromoteUserToAdminRequest, User> _promoteUserToAdminRequestHandler;
        public UserController(IRequestHandler<GetAllUsersRequest, IList<User>> getAllUsersRequestHandler,
                                IRequestHandler<GetUserRequest, User> getUserRequestHandler,
                                IRequestHandler<RegisterUserRequest, User> registerUserRequestHandler,
                                IRequestHandler<UpdateUserRequest, User> updateUserRequestHandler,
                                IRequestHandler<AddUserToClientRequest, bool> addUserToClientRequestHandler,
                                IRequestHandler<RemoveUserFromClientRequest, bool> removeUserFromClientRequestHandler,
                                IRequestHandler<PromoteUserToAdminRequest, User> promoteUserToAdminRequestHandler)
        {
            _getAllUsersRequestHandler = getAllUsersRequestHandler;
            _getUserRequestHandler = getUserRequestHandler;
            _registerUserRequestHandler = registerUserRequestHandler;
            _updateUserRequestHandler = updateUserRequestHandler;
            _addUserToClientRequestHandler = addUserToClientRequestHandler;
            _removeUserFromClientRequestHandler = removeUserFromClientRequestHandler;
            _promoteUserToAdminRequestHandler = promoteUserToAdminRequestHandler;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] string userId)
        {
            var user = await _getUserRequestHandler.HandleRequest(new GetUserRequest(userId));
            if (user == null)
                return BadRequest($"Could not find user with the ID of: {userId}");
            return Ok(user);
        }

        [HttpPost]
        [Route("Users")]
        public async Task<IActionResult> GetAll(GetAllUsersHttpRequest request)
        {
            var users = await _getAllUsersRequestHandler.HandleRequest(new GetAllUsersRequest(
                request.ClientId,
                request.Email,
                request.EmailExact,
                request.FirstName,
                request.FirstNameExact,
                request.LastName,
                request.LastNameExact,
                request.IncludeDeleted));
            return Ok(users);
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register(RegisterUserHttpRequest request)
        {
            return Ok(await _registerUserRequestHandler.HandleRequest(new RegisterUserRequest
            {
                Email = request.Email,
                FirstName = request.FirstName, 
                LastName = request.LastName,
                HashSalt = new SaltedHash.HashSalt
                {
                    Hash = request.Hash,
                    Salt = request.Salt
                }
            }));
        }

        [HttpPut]
        public async Task<IActionResult> UpdateUser(UpdateUserHttpRequest request)
        {
            var result = await _updateUserRequestHandler.HandleRequest(new UpdateUserRequest
            {
                UserId = request.UserId,
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Salt = request.Salt,
                Hash = request.Hash,
                Deleted = request.Deleted,
                Role = request.Role
            });

            if (result == null)
                return BadRequest($"Could not find user with the ID of: {request.UserId}");
            return Ok(result);
        }

        [HttpPatch]
        [Route("JoinTeam")]
        public async Task<IActionResult> JoinTeam([FromQuery] string userId, [FromQuery] string clientJoinToken)
        {
            var result = await _addUserToClientRequestHandler.HandleRequest(new AddUserToClientRequest { UserId = userId, ClientJoinToken = clientJoinToken });
            if (!result)
                return BadRequest($"Either Could not find client with the Join Token of: {clientJoinToken} or Could not find user with the ID of: {userId}");
            return Ok();
        }

        [HttpPatch]
        [Route("LeaveTeam")]
        public async Task<IActionResult> LeaveTeam([FromQuery] string currentUser, [FromQuery] string userId, [FromQuery] string clientId)
        {
            var result = await _removeUserFromClientRequestHandler.HandleRequest(new RemoveUserFromClientRequest{ClientId = clientId, CurrentUserId = currentUser, UserId = userId});
            if (!result)
                return BadRequest($"Either Could not find client with the ID of: {clientId} or Could not find user with the ID of: {userId}");
            return Ok();
        }

        //[HttpPatch]
        //[Route("UpdateStatus")]
        //public async Task<IActionResult> UpdateUserStatus([FromQuery] string userId, [FromQuery] bool userEnabled)
        //{
        //    var result = await _updateUserStatusRequestHandler.HandleRequest(new UpdateUserStatusRequest(userId, userEnabled));
        //    if (result == null)
        //        return BadRequest($"User with ID of: {userId} cannot be found!");
        //    return Ok(result);
        //}

        [HttpPatch]
        [Route("Promote")]
        public async Task<IActionResult> PromoteUser([FromQuery] string userId)
        {
            var result = await _promoteUserToAdminRequestHandler.HandleRequest(new PromoteUserToAdminRequest{UserId = userId});
            if (result == null)
                return BadRequest($"User with ID of: {userId} cannot be found!");
            return Ok(result);
        }
    }
}
