﻿using System.ComponentModel.DataAnnotations;


namespace AuthenticationService.Controllers.HttpRequests
{
    public class UpdateUserHttpRequest
    {
        [Required]
        public string UserId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool? Deleted { get; set; }
        public string Role { get; set; }
        public string Hash { get; set; }
        public string Salt { get; set; }
    }
}
