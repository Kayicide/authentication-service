﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenticationService.Controllers.HttpRequests
{
    public class UpdateJwtHttpRequest
    {
        public string CurrentUser { get; set; }
        public string ClientId { get; set; }
        public string Jwt { get; set; }
    }
}
