﻿namespace AuthenticationService.Controllers.HttpRequests
{
    public class UpdateClientHttpRequest
    {
        public string CurrentUser { get; set; }
        public string ClientId { get; set; }
        public string Name { get; set; }
        public bool? Active { get; set; }
    }
}
