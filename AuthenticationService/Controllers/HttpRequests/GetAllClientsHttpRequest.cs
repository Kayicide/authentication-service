﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenticationService.Controllers.HttpRequests
{
    public class GetAllClientsHttpRequest
    {
        public string UserId { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public bool NameExact { get; set; } = false;
        public string JoinToken { get; set; } = string.Empty;
        public bool? Active { get; set; } = false;
        public DateTime LowerCreatedDateTime { get; set; } = DateTime.MinValue;
        public DateTime UpperCreatedDateTime { get; set; } = DateTime.MinValue;
    }
}
