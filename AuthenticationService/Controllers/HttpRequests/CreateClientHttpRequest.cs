﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Data.Entity;

namespace AuthenticationService.Controllers.HttpRequests
{
    public class CreateClientHttpRequest
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public ClientType Type { get; set; }
        [Required]
        public string UserId { get; set; }
    }
}
