﻿namespace AuthenticationService.Controllers.HttpRequests
{
    public class GetAllUsersHttpRequest
    {
        public string ClientId { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public bool EmailExact { get; set; } = false;
        public string FirstName { get; set; } = string.Empty;
        public bool FirstNameExact { get; set; } = false;
        public string LastName { get; set; } = string.Empty;
        public bool LastNameExact { get; set; } = false;
        public bool IncludeDeleted { get; set; } = false;
    }
}
