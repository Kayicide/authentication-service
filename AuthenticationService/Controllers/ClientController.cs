﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;
using AuthenticationService.Controllers.HttpRequests;
using AuthenticationService.Data.Entity;
using AuthenticationService.Features;
using AuthenticationService.Features.ClientFeature;
using LoggingService.Features;
using Microsoft.AspNetCore.Authorization;

namespace AuthenticationService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class ClientController : Controller
    {
        private readonly IRequestHandler<CreateClientRequest, Client> _createClientRequestHandler;
        private readonly IRequestHandler<GetAllClientsRequest, IList<Client>> _getAllClientsRequestHandler;
        private readonly IRequestHandler<GetClientRequest, Client> _getClientRequestHandler;
        private readonly IRequestHandler<RegenerateJoinTokenRequest, Client> _regenerateJoinTokenRequestHandler;
        private readonly IRequestHandler<AddAdminRequest, Client> _addAdminRequestHandler;
        private readonly IRequestHandler<RemoveAdminRequest, Client> _removeAdminRequestHandler;
        private readonly IRequestHandler<UpdateClientNameRequest, Client> _updateClientNameRequestHandler;
        private readonly IRequestHandler<SwitchOwnershipRequest, Client> _switchOwnershipRequestHandler;
        private readonly IRequestHandler<UpdateClientJwtTokenRequest, Client> _updateClientJwtRequestHandler;

        public ClientController(IRequestHandler<CreateClientRequest, Client> createClientRequestHandler,
                                    IRequestHandler<GetAllClientsRequest, IList<Client>> getAllClientsRequestHandler,
                                    IRequestHandler<GetClientRequest, Client> getClientRequestHandler,
                                    IRequestHandler<RegenerateJoinTokenRequest, Client> regenerateJoinTokenRequestHandler,
                                    IRequestHandler<AddAdminRequest, Client> addAdminRequestHandler,
                                    IRequestHandler<RemoveAdminRequest, Client> removeAdminRequestHandler,
                                    IRequestHandler<UpdateClientNameRequest, Client> updateClientNameRequestHandler,
                                    IRequestHandler<SwitchOwnershipRequest, Client> switchOwnershipRequestHandler,
                                    IRequestHandler<UpdateClientJwtTokenRequest, Client> updateClientJwtRequestHandler)
        {
            _createClientRequestHandler = createClientRequestHandler;
            _getAllClientsRequestHandler = getAllClientsRequestHandler;
            _getClientRequestHandler = getClientRequestHandler;
            _regenerateJoinTokenRequestHandler = regenerateJoinTokenRequestHandler;
            _addAdminRequestHandler = addAdminRequestHandler;
            _removeAdminRequestHandler = removeAdminRequestHandler;
            _updateClientNameRequestHandler = updateClientNameRequestHandler;
            _switchOwnershipRequestHandler = switchOwnershipRequestHandler;
            _updateClientJwtRequestHandler = updateClientJwtRequestHandler;
        }
        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] CreateClientHttpRequest request)
        {
            var client = await _createClientRequestHandler.HandleRequest(new CreateClientRequest { UserId = request.UserId, Type = request.Type, Name = request.Name});
            if (client == null)
                return Problem("Error While Creating Client");
            return Ok(client);
        }

        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Update([FromBody] UpdateClientHttpRequest request)
        {
            var client = await _updateClientNameRequestHandler.HandleRequest(new UpdateClientNameRequest{ClientId = request.ClientId, Name = request.Name, CurrentUserId = request.CurrentUser, Active = request.Active});
            if (client == null)
                return BadRequest($"Could not find client with the ID of: {request.ClientId}");
            return Ok(client);
        }

        [Route("UpdateJwt")]
        [HttpPost]
        public async Task<IActionResult> UpdateJwt([FromBody] UpdateJwtHttpRequest request)
        {
            var client = await _updateClientJwtRequestHandler.HandleRequest(new UpdateClientJwtTokenRequest{ClientId = request.ClientId, CurrentUserId = request.CurrentUser, JwtToken = request.Jwt});
            if (client == null)
                return BadRequest($"Could not find client with the ID of: {request.ClientId}");
            return Ok(client);
        }

        [Route("RegenJoinToken")]
        [HttpPatch]
        public async Task<IActionResult> RegenerateJoinToken([FromQuery] string currentUser, [FromQuery] string clientId)
        {
            var client = await _regenerateJoinTokenRequestHandler.HandleRequest(new RegenerateJoinTokenRequest{ClientId = clientId, CurrentUserId = currentUser});
            if (client == null)
                return BadRequest($"Could not find client with the ID of: {clientId}");
            return Ok(client);
        }

        [Route("AddAdmin")]
        [HttpPatch]
        public async Task<IActionResult> AddAdmin([FromQuery] string currentUser, [FromQuery] string clientId, [FromQuery] string userId)
        {
            var client = await _addAdminRequestHandler.HandleRequest(new AddAdminRequest{ClientId = clientId, UserId = userId, CurrentUser = currentUser});
            if (client == null)
                return BadRequest($"Either Could not find client with the ID of: {clientId} or Could not find user with the ID of: {userId}");
            return Ok(client);
        }

        [Route("RemoveAdmin")]
        [HttpPatch]
        public async Task<IActionResult> RemoveAdmin([FromQuery] string currentUser, [FromQuery] string clientId, [FromQuery] string userId)
        {
            var client = await _removeAdminRequestHandler.HandleRequest(new RemoveAdminRequest{ClientId = clientId, UserId = userId, CurrentUser = currentUser});
            if (client == null)
                return BadRequest($"Either Could not find client with the ID of: {clientId} or Could not find user with the ID of: {userId}");
            return Ok(client);
        }

        [Route("TransferOwnership")]
        [HttpPatch]
        public async Task<IActionResult> SwitchOwnership([FromQuery] string currentUser, [FromQuery] string clientId, [FromQuery] string userId)
        {
            var client = await _switchOwnershipRequestHandler.HandleRequest(new SwitchOwnershipRequest{ClientId = clientId, UserId = userId, CurrentUser = currentUser});
            if (client == null)
                return BadRequest($"Either Could not find client with the ID of: {clientId} or Could not find user with the ID of: {userId}");
            return Ok(client);
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] string clientId)
        {
            var client = await _getClientRequestHandler.HandleRequest(new GetClientRequest { ClientId = clientId });
            if (client == null)
                return BadRequest($"Could not find client with the ID of: {clientId}");
            return Ok(client);
        }

        [HttpPost]
        [Route("clients")]
        public async Task<IActionResult> GetAll([FromBody] GetAllClientsHttpRequest request)
        {
            var clients = await _getAllClientsRequestHandler.HandleRequest(new GetAllClientsRequest
            {
                UserId = request.UserId,
                Name = request.Name,
                NameExact = request.NameExact,
                JoinToken = request.JoinToken,
                Active = request.Active,
                UpperCreatedDateTime = request.UpperCreatedDateTime,
                LowerCreatedDateTime = request.LowerCreatedDateTime
            });
            return Ok(clients);
        }
    }
}
